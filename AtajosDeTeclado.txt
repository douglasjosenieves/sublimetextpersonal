[
 {"keys": ["f1"], "command": "reindent", "args": {"single_line": false}},
 { "keys": ["ctrl+1"], "command": "toggle_side_bar" },
 { "keys": ["f6"], "command": "expand_fqcn" },
 { "keys": ["shift+f6"], "command": "expand_fqcn", "args": {"leading_separator": true} },
 { "keys": ["f5"], "command": "find_use" },
 { "keys": ["f4"], "command": "import_namespace" },
 { "keys": ["f3"], "command": "implement" },
 { "keys": ["shift+f12"], "command": "goto_definition_scope" },
 { "keys": ["f7"], "command": "insert_php_constructor_property" },
 { "keys": ["f2"], "command": "toggle_comment", "args": { "block": false } },
 { "keys": ["f3"], "command": "toggle_comment", "args": { "block": true } },

{ "keys": ["ctrl+."], "command": "open_dir", "args": {"dir": "$file_path", "file": "$file_name"} },
 

 	    {
        "keys": ["ctrl+2"],


        "command": "reveal_in_side_bar"
    },

    { "keys": ["alt+c,alt+d"], "command": "build", "args": { "file": "{packages}/User/cmdRunFromDIR.sublime-build" } }


]
